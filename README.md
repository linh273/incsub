## Folder Structure

```
incsub/
    README.md
    node_modules/
    package.json
    webpack.config.js
    tsconfig.json
    public/
        index.html
        favicon.ico
    src/
        index.scss
        index.tsx
        components/
        containers/
        hooks/
        types/
        utils/
```

## How to pass all cases
1. Product name is required.
2. At least 1 plan to be added
    - At beginning, `Add another plan` will be disabled.
    - Enter `Plan name`, valid other fields will make `Add another plan` enable again.
    - After clicking `Add another plan`, It will reset the form.

3. After pass 2 above requirements, `Create` button will be enabled.

## Deployment page:
Page: `https://linh273.gitlab.io/incsub/`
Using CI/CD on gitlab