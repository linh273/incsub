import React, { useRef, useState } from 'react'
import { DownArrowIcon, MoreIcon } from '..';
import useOnClickOutside from '../../hooks/useClickOutSide';
import { IPlan } from '../../types/product';
import "./Plan.scss";

interface IProps {
    plan: IPlan,
    duplicateCallBack: (plan: IPlan) => void,
    archiveCallBack: (plan: IPlan) => void
}

const Plan: React.FC<IProps> = ({
    plan,
    duplicateCallBack,
    archiveCallBack
}) => {
    const {name, price} = plan;
    const [isOpenDropdown, setIsOpenDropdown] = useState<boolean>(false);
    const ref = useRef<HTMLDivElement>(null);

    useOnClickOutside(ref, () => {
        if (ref.current) {
            setIsOpenDropdown(false);
        }
    });

    const handleDuplicatePlan = (e: React.FormEvent<HTMLSpanElement>) => {
        e.stopPropagation();
        duplicateCallBack({
            ...plan,
            createdTime: (new Date()).getTime()
        });
        setIsOpenDropdown(false);
    }

    const handleArchivePlan = () => {
        archiveCallBack(plan);
        setIsOpenDropdown(false);
    }

    return (
        <div className="plan__item">
            <span className="plan__item-name">
                {name}
            </span>
            <span className="plan__item-status">
                Active
            </span>
            <span className="plan__item-price">
                $ {price.toFixed(2)}
            </span>
            <div
                className={`plan__item-more ${isOpenDropdown ? "open" : ""}`}
                onClick={() => setIsOpenDropdown(prev => !prev)}
                ref={ref}
            >
                <MoreIcon />

                {isOpenDropdown &&
                    <div
                        className="dropdown__content"
                    >
                        <p
                            className="dropdown__option"
                            onClick={handleDuplicatePlan}
                        >
                            Duplicate Plan
                        </p>
                        <p
                            className="dropdown__option archive"
                            onClick={handleArchivePlan}
                        >
                            Archive Plan
                        </p>
                    </div>
                }
            </div>
            <span className="plan__item-expand">
                <DownArrowIcon
                    height="15"
                    width="15"
                    fontWeight="700"
                />
            </span>
        </div>
    )
}

export default Plan;
