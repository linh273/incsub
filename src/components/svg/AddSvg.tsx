import * as React from "react";
import { SVGProps } from "react";

const AddSvg = (props: SVGProps<SVGSVGElement>) => (
    <svg
        width={12}
        height={12}
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <path
            d="M11.449 4.971h-4.42V.551a.535.535 0 0 0-.16-.392A.533.533 0 0 0 6.478 0h-.956a.56.56 0 0 0-.391.159.535.535 0 0 0-.16.392v4.42H.551a.513.513 0 0 0-.392.172.518.518 0 0 0-.159.38v.955a.56.56 0 0 0 .159.391c.106.106.237.16.392.16h4.42v4.42c0 .155.054.286.16.392a.56.56 0 0 0 .391.159h.956a.533.533 0 0 0 .391-.159.535.535 0 0 0 .16-.392v-4.42h4.42a.535.535 0 0 0 .392-.16.56.56 0 0 0 .159-.391v-.956a.518.518 0 0 0-.159-.379.513.513 0 0 0-.392-.172Z"
            fill={props.fill ? props.fill : "#286EF1"}
        />
    </svg>
)

export default AddSvg;
