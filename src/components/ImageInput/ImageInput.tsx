import React, { useEffect, useState } from 'react'
import { AddIcon } from '..';
import { IImage } from '../../types/product';
import "./ImageInput.scss";

interface IProps {
    callback: (image: IImage) => void
}

const ImageInput: React.FC<IProps> = ({ callback }) => {
    const [image, setImage] = useState<IImage>(null);

    useEffect(() => {
        return () => {
            image && URL.revokeObjectURL(image.imageUrl);
        }
    }, [image]);

    const handleImageChange = (e: React.FormEvent<HTMLInputElement>) => {
        const file = e.currentTarget.files[0];
        const imageUrl = URL.createObjectURL(file);
        const image: IImage = {
            imageFile: file,
            imageUrl: imageUrl
        }
        setImage(image);
        callback(image);

    }

    const handleRemoveImage = () => {
        setImage(null);
        callback(null);
    }

    return (
        <>
            { image &&
                <div className="product-image__container">
                    <img src={image.imageUrl} alt="productImage" />
                    <label
                        className="btn btn-edit"
                        htmlFor="product-image"
                    >
                        Edit
                    </label>
                    <button
                        className="btn btn-remove"
                        onClick={handleRemoveImage}
                    >
                        Remove
                    </button>
                </div>
            }
            <>
                <label
                    htmlFor="product-image"
                    className="product-image"
                    style={{display: image && "none"}}
                >
                    <AddIcon />
                </label>
                <input
                    type="file"
                    id="product-image"
                    style={{display: "none"}}
                    accept="image/*"
                    onChange={handleImageChange}
                />
            </>
        </>
    )
}

export default ImageInput
