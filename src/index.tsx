import React from 'react';
import ReactDOM from 'react-dom/client';
import NewProduct from './containers/Product/NewProduct';
import "./index.scss"

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <NewProduct />
    </React.StrictMode>
)
