import { BillingEveryEnum, BillingTypeEnum } from "../utils/enums";

declare namespace Product {
    interface IProduct {
        name: string,
        image?: IImage,
        plans: IPlan[]
    }
    interface IPlan {
        name: string,
        price: number,
        billingType: IBillingType,
        billEveryNumber: number,
        billEveryType: IBillEvery,
        noOfCycles ?: number,
        createdTime : number
    }
    
    interface IImage {
        imageFile: File,
        imageUrl: string
    }

    interface IBillingType {
        name: string,
        type: BillingType,
        text: string
    }

    interface IBillEvery {
        value: BillEveryType,
        text: string,
    }

    interface IValid {
        isValid: boolean,
        text?: string
    }

    type BillEveryType = BillingEveryEnum.MONTH | BillingEveryEnum.YEAR;
    type BillingType = BillingTypeEnum.RECURRING | BillingTypeEnum.ONE_TIME;
}

export = Product;
