import { useState } from 'react';
import { IProduct, IValid } from '../../types/product';

const useProductValidation = (product: IProduct): IValid => {
    if (product.name.trim().length <= 0) {
        return {
            isValid: false,
            text: "Product name is required"
        }
    }

    if (product.plans.length === 0) {
        return {
            isValid: false,
            text: "Should have at least 1 plan"
        }
    }

    return {
        isValid: true,
    }
}

export default useProductValidation;
