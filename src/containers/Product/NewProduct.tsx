import React, { useCallback, useState } from "react";
import { ImageInput, Plan } from "../../components";
import { IImage, IPlan, IProduct } from "../../types/product";
import PlanDetail from "./PlanDetail";
import "./NewProduct.scss";
import useProductValidation from "./useProductValidation";

const NewProduct: React.FC = () => {
    const [product, setProduct] = useState<IProduct>({
        name: "",
        image: null,
        plans: []
    });

    const {isValid} = useProductValidation(product);

    const imageChangeCallBack = useCallback((image: IImage) => {
        setProduct(prev => {
            return {
                ...prev,
                image: image
            }
        });
    }, [product.image]);
    
    const addPlanCallBack = useCallback((plan: IPlan) => {
        setProduct(prev => {
            return {
                ...prev,
                plans: [...prev.plans, plan]
            }
        });
    }, []);

    const duplicatePlanCallBack = useCallback((plan: IPlan) => {
        setProduct(prev => {
            return {
                ...prev,
                plans: [...prev.plans, plan]
            }
        });
    }, []);

    const archivePlanCallBack = useCallback((plan: IPlan) => {
        setProduct(prev => {
            return {
                ...prev,
                plans: prev.plans.filter(item => item.createdTime !== plan.createdTime)
            }
        });
    }, []);

    const handleProductNameChange = (e: React.FormEvent<HTMLInputElement>) => {
        const name = e.currentTarget.value;
        setProduct(prev => {
            return {
                ...prev,
                name: name
            }
        })
    }
    

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        console.log("DO something with submit", product);
        alert(JSON.stringify(product, null, 2))
    }

    const { name } = product;
    
    return (
        <form
            className="product__container"
            onSubmit={handleSubmit}
        >
            <h2>Create Product or Service</h2>

            {/* General Info */}
            <div className="info">
                <h4>
                    General Info
                </h4>
                <div className="info__block">
                    <div className="text-input__container">
                        <label htmlFor="product-name">
                            Product Name
                        </label>
                        <input
                            type="text"
                            id="product-name"
                            placeholder="E.g. Website Maintenance, SEO, etc."
                            value={name}
                            onChange={handleProductNameChange}
                        />
                    </div>
                    <div className="text-input__container">
                        <label htmlFor="product-image">
                            Product Image
                            <span className="optional">(Optional)</span>
                        </label>
                        <ImageInput callback={(value) => imageChangeCallBack(value)} />
                        <p className="text-input__description">
                            Upload a sqaure image that doesn’t exceed 2 MB.
                        </p>
                    </div>
                </div>
            </div>

            {/* Pricing Plans */}

            <div className="info">
                <h4>
                    Pricing Plans
                </h4>
                <p className="info__description">
                    Create pricing plans for this product/service. Note that every product/service can have multiple plans.
                </p>

                {/* Plan item */}
                { product.plans.map((plan: IPlan) => (
                    <Plan
                        key={plan.createdTime}
                        plan={plan}
                        duplicateCallBack={(item) => duplicatePlanCallBack(item)}
                        archiveCallBack={(item) => archivePlanCallBack(item)}
                    />
                ))}
                <PlanDetail callback={(plan) => addPlanCallBack(plan)}/>
            </div>

            {/* SUBMIT */}
            <div className="btn__container">
                <button
                    className="btn btn-cancel"
                    type="button"
                >
                    Cancel
                </button>
                <button
                    className={`btn btn-create ${!isValid ? "disabled" : ""}`}
                    disabled={!isValid}
                    type="submit"
                >
                    Create
                </button>
            </div>
        </form>
    )
}

export default NewProduct
