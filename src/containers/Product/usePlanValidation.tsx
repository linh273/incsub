import { IPlan, IValid } from '../../types/product';
import { BillingTypeEnum } from '../../utils/enums';

const usePlanValidation = (plan: IPlan): IValid => {
    
    if (plan.name.length <= 0) {
        return {
            isValid: false,
            text: "Name is required"
        }
    }

    if (isNaN(plan.price) || plan.price < 0) {
        return {
            isValid: false,
            text: "Price must be equal or higher than 0"
        }
    }

    if (
        plan.billingType.type === BillingTypeEnum.RECURRING 
        && (
            isNaN(plan.billEveryNumber) || plan.billEveryNumber < 0
        )
    ) {
        return {
            isValid: false,
            text: "Invalid Bill Every"
        }
    }

    if (
        plan.billingType.type === BillingTypeEnum.RECURRING 
        && !isNaN(plan.noOfCycles) && plan.noOfCycles < 0
    ) {
        return {
            isValid: false,
            text: "Invalid No Of Cycles"
        }
    }
    
    return {
        isValid: true
    };
}

export default usePlanValidation;
