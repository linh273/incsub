import React, { useRef, useState } from 'react'
import { AddIcon, DownArrowIcon } from '../../components';
import useOnClickOutside from '../../hooks/useClickOutSide';
import { IBillEvery, IBillingType, IPlan } from '../../types/product';
import { BillingEveryEnum, BillingTypeEnum } from '../../utils/enums';
import "./PlanDetail.scss";
import usePlanValidation from './usePlanValidation';

interface IProps {
    callback: (plan: IPlan) => void
}

export const billingTypeArray: IBillingType[] = [
    {
        name: "recurring",
        type: BillingTypeEnum.RECURRING,
        text: "Recurring"
    },
    {
        name: "oneTime",
        type: BillingTypeEnum.ONE_TIME,
        text: "One time"
    }
]

export const billEveryArray: IBillEvery[] = [
    {
        value: BillingEveryEnum.MONTH,
        text: "Month(s)"
    },
    {
        value: BillingEveryEnum.YEAR,
        text: "Year(s)"
    }
];

const defaultPlan: IPlan = {
    name: "",
    price: 0,
    billingType: billingTypeArray[0],
    billEveryNumber: 1,
    billEveryType: billEveryArray[0],
    createdTime: 0,
    noOfCycles: 0
};

const PlanDetail: React.FC<IProps> = ({ callback }) => {
    const [plan, setPlan] = useState<IPlan>(defaultPlan);
    const [isOpenBill, setIsOpenBill] = useState<boolean>(false);
    const billEveryRef = useRef<HTMLDivElement>(null);
    const {isValid} = usePlanValidation(plan);

    useOnClickOutside(billEveryRef, () => {
        if (billEveryRef.current) {
            setIsOpenBill(false);
        }
    });

    const handlePlanNameChange = (e: React.FormEvent<HTMLInputElement>) => {
        const currentValue = e.currentTarget.value;

        setPlan(prev => {
            return {
                ...prev,
                name: currentValue
            }
        })
    }

    const handlePriceChange = (e: React.FormEvent<HTMLInputElement>) => {
        const currentValue = e.currentTarget.value;
        
        setPlan(prev => {
            return {
                ...prev,
                price: parseFloat(currentValue)
            }
        })
    }

    const handleBillingTypeChange = (type: IBillingType) => {        
        setPlan(prev => {
            return {
                ...prev,
                billingType: type
            }
        })
    }

    const handleBillEverySelect = (type: IBillEvery) => {
        setPlan(prev => {
            return {
                ...prev,
                billEveryType: type
            }
        })
        setIsOpenBill(false);
    }

    const handleNoOfBillingCycles = (e: React.FormEvent<HTMLInputElement>) => {
        const currentValue = e.currentTarget.value;
        setPlan(prev => {
            return {
                ...prev,
                noOfCycles: parseInt(currentValue)
            }
        })
    }

    const handleBillEveryChange = (e: React.FormEvent<HTMLInputElement>) => {
        const currentValue = e.currentTarget.value;

        setPlan(prev => {
            return {
                ...prev,
                billEveryNumber: parseInt(currentValue)
            }
        })
    }

    const handleAddPlan = () => {
        callback({
            ...plan,
            createdTime: (new Date()).getTime()
        });
        setPlan(defaultPlan);
    }

    const {
        name,
        price,
        billingType,
        billEveryType,
        billEveryNumber,
        noOfCycles
    } = plan;

    return (
        <>
            <div className="info__block info__block-first">
                {/* Plan name */}
                <div className="text-input__container">
                    <label htmlFor="plan-name">
                        Plan Name
                    </label>
                    <input
                        type="text"
                        id="plan-name"
                        placeholder="E.g. Monthly, Lifetime, etc."
                        value={name}
                        onChange={handlePlanNameChange}
                    />
                </div>

                {/* Billing Type */}
                <div className="text-input__container">
                    <label>
                        Billing Type
                    </label>
                    <div className="billing-type__container">
                        {billingTypeArray.map((item: IBillingType) => (
                            <div key={item.name}>
                                <label
                                    htmlFor={item.name}
                                    className={`tab ${billingType.type === item.type && "active"}`}
                                >
                                    {item.text}
                                </label>
                                <input
                                    type="checkbox"
                                    name={item.name}
                                    id={item.name}
                                    style={{ display: "none" }}
                                    onChange={() => handleBillingTypeChange(item)}
                                    checked={billingType.type === item.type}
                                />
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            <div className="info__block">
                {/* Price */}
                <div className="text-input__container">
                    <label htmlFor="price">
                        Price
                    </label>
                    <div className="price__container">
                        <input
                            id="price"
                            type="number"
                            min="0.00"
                            step="0.01"
                            max="2500"
                            value={price === 0 ? "0.00" : price}
                            onChange={handlePriceChange}
                        />
                        <span>USD</span>
                    </div>
                </div>

                {/* Bill Every */}

                {billingType.type === BillingTypeEnum.RECURRING &&
                    <div className="text-input__container">
                        <label htmlFor="bill-every">
                            Bill Every
                        </label>
                        <div className="bill__container" ref={billEveryRef}>
                            <input
                                type="number"
                                min="1"
                                step="1"
                                max="2500"
                                id="bill-every"
                                value={billEveryNumber.toString()}
                                onChange={handleBillEveryChange}
                            />

                            <button
                                className="dropdown-btn"
                                onClick={() => setIsOpenBill(prev => !prev)}
                                type="button"
                            >
                                {billEveryType.text}
                                <DownArrowIcon
                                    height="13"
                                    width="7"
                                    fontWeight="700"
                                />
                            </button>
                            {isOpenBill &&
                                <div className="dropdown__content">
                                    {billEveryArray.map((item: IBillEvery) => (
                                        <p
                                            className="dropdown__option"
                                            key={item.value}
                                            onClick={() => handleBillEverySelect(item)}
                                        >
                                            {item.text}
                                        </p>
                                    ))}
                                </div>
                            }
                        </div>
                    </div>
                }
            </div>

            {/* No. of Billing Cycles */}
            {billingType.type === BillingTypeEnum.RECURRING &&
                <div className="info__block">
                    <div className="text-input__container">
                        <label htmlFor="billing-cycles">
                            No. of Billing Cycles
                            <span className="optional">(Optional)</span>
                        </label>
                        <input
                            id="billing-cycles"
                            type="number"
                            min="0"
                            step="1"
                            max="2500"
                            value={noOfCycles === 0 ? "" : noOfCycles}
                            placeholder="E.g. 6, 12, etc."
                            onChange={handleNoOfBillingCycles}
                        />
                        <p className="text-input__description">
                            Leave this empty to auto-renew this plan until canceled.
                        </p>
                    </div>
                </div>
            }

            <div className="info__block">
                <div className="text-input__container">
                    <button
                        type="button"
                        className={`btn another-plan-btn ${isValid ? "" : "disabled"}`}
                        disabled={!isValid}
                        onClick={handleAddPlan}
                    >
                        <AddIcon fill="#000" />
                        Add Another Plan
                    </button>
                </div>
            </div>
        </>
    )
}

export default PlanDetail;
