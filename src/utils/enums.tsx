export enum BillingTypeEnum {
    RECURRING = "recurring",
    ONE_TIME = "onetime"
}

export enum BillingEveryEnum {
    MONTH = "month",
    YEAR = "year"
}