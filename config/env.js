const fs = require('fs')
const dotenv = require('dotenv')
const dotenvExpand = require('dotenv-expand')

const dotenvFiles = [
    "env.local",
    "env"
];

dotenvFiles.forEach((dotenvFile) => {
    if (fs.existsSync(dotenvFile)) {
        dotenvExpand.expand(dotenv.config({
            path: dotenvFile
        }))
    }
})

const REACT_APP = /^REACT_APP_/

module.exports = function getClientEnvironment(publicUrl) {
    const raw = Object.keys(process.env)
        .filter((key) => REACT_APP.test(key))
        .reduce(
            (env, key) => ({
                ...env,
                [key]: process.env[key]
            }), {
                NODE_ENV: process.env.NODE_ENV || 'development',
                PUBLIC_URL: publicUrl,
            },
        )
    const stringified = {
        'process.env': Object.keys(raw).reduce((env, key) => ({
            ...env,
            [key]: JSON.stringify(raw[key])
        }), {}),
    }

    return {
        raw,
        stringified
    }
};