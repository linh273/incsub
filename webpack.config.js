const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require('webpack')
const path = require("path");
const getClientEnvironment = require("./config/env");
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const devMode = process.env.NODE_ENV !== "production";
const mediaInlineSizeLimit = 10240;
const publicPath = process.env.PUBLIC_PATH || '/';
const env = getClientEnvironment(publicPath);

module.exports = {
    devtool: devMode ? 'cheap-module-source-map' : false,
    entry: "./src/index.tsx",
    output: {
        path: path.join(__dirname, "/build"),
        filename: devMode ? 'static/js/bundle.js' : 'static/js/[name].[contenthash:8].js',
        chunkFilename: devMode ? 'static/js/[name].chunk.js' : 'static/js/[name].[contenthash:8].chunk.js',
        assetModuleFilename: 'static/media/[name].[hash:8][ext]',
        clean: true,
        publicPath,
    },
    module: {
        rules: [
        {
            oneOf: [{
                    test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                    type: 'asset',
                    parser: {
                        dataUrlCondition: {
                            maxSize: mediaInlineSizeLimit,
                        },
                    },
                },
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    use: ['babel-loader'],
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'sass-loader',
                    ],
                },
                {
                    exclude: [/^$/, /\.(js|jsx|tsx|ts)$/, /\.html$/, /\.json$/],
                    type: 'asset/resource',
                }
            ],

        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./public/index.html",
            favicon: './public/favicon.ico',
            publicPath,
        }),
        new webpack.DefinePlugin(env.stringified),
    ].concat(devMode ? [] : [new MiniCssExtractPlugin({
        filename: 'static/css/[name].[contenthash:8].css',
        chunkFilename: 'static/css/[name].[contenthash:8].chunk.css',
    })]),
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
    }
};